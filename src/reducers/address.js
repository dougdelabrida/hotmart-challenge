import {
  FETCH_REQUEST,
  RECEIVE_ADDRESSES,
  RECEIVE_ADDRESS,
  EDIT_ADDRESS_SUCCESS,
  ADD_ADDRESS_SUCCESS,
  ADD_ADDRESS_ERROR,
  DELETE_ADDRESS_SUCCESS
} from '../actions/address';

export default(state = {
  isFetching: false,
  data: [],
  selected: {},
  message: {}
}, payload) => {
    switch (payload.type) {
      case FETCH_REQUEST:
        return Object.assign({}, state, {
          isFetching: true
        });
      case RECEIVE_ADDRESSES:
        const {size, page, data} = payload.result;
        return Object.assign({}, state, {
          isFetching: false,
          size,
          page,
          data
        });
      case RECEIVE_ADDRESS:
        return Object.assign({}, state, {
          selected: payload.data,
          isFetching: false
        });
      case EDIT_ADDRESS_SUCCESS:
        Object.keys(payload.address).forEach(key => state.selected[key] = payload.address[key]);
        return Object.assign({}, state, {
          isFetching: false
        });
      case ADD_ADDRESS_SUCCESS:
        return Object.assign({}, state, {
          isFetching: false,
          data: [...state.data, payload.address],
          message: {
            type: 'success',
            text: 'Endereço cadastrado com sucesso'
          }
        });
      case ADD_ADDRESS_ERROR:
        return Object.assign({}, state, {
          isFetching: false,
          message: {
            type: 'error',
            text: payload.message
          }
        });
      case DELETE_ADDRESS_SUCCESS:
        return Object.assign({}, state, {
          data: state.data.filter((e, i) => i === payload.index),
          message: {
            type: 'success',
            text: 'Endereço removido com sucesso.'
          }
        });
      default:
        return state;
    }
};
