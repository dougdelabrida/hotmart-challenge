import user from './user';
import address from './address';
import { combineReducers } from 'redux';
const rootReducer = combineReducers({
  user,
  address
});
export default rootReducer;
