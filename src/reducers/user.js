import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE
} from '../actions/user';

export default(state = {
  isFetching: false,
  isAuth: localStorage.getItem('auth_data') !== null,
  authData: JSON.parse(localStorage.getItem('auth_data')),
  error: {}
}, payload) => {
    switch (payload.type) {
        case USER_LOGIN_REQUEST:
          return Object.assign({}, state, {
            isFetching: true
          });
        case USER_LOGIN_SUCCESS:
          localStorage.setItem('auth_data', JSON.stringify(payload.data));
          return Object.assign({}, state, {
            isFetching: false,
            isAuth: true,
            authData: payload.data
          });
        case USER_LOGIN_FAILURE:
          return Object.assign({}, state, {
            isFetching: false,
            error: payload.error
          })
        default:
          return state;
    }
};
