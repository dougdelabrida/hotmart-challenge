import React from 'react';
import { Link } from 'react-router';
import './styles/TopBar.css';

const Logout = (e) => {
  e.preventDefault();
  localStorage.removeItem('auth_data');
  location.reload();
}

const TopBar = ({backTo}) => (
  <div className='top-bar custom stacked-for-medium'>
    <div className=' left' style={{display: backTo ? 'block' : 'none'}}>
      <Link className='action-back' to={backTo}>❮</Link>
    </div>

    <div className='right logout'>
      <a href='/#' onClick={(e) => Logout(e)}>Sair</a>
    </div>

  </div>
)

export default TopBar;
