import React from 'react';
import { Link } from 'react-router';
import './styles/AddressItem.css';

class AddressItem extends React.Component {
  state = { showActions: false }

  toggleActions = (e) => this.setState({showActions: !this.state.showActions});

  render() {
    const { item, onDelete } = this.props;
    const { showActions } = this.state;
    return (
      <div className='card'>
        <Link to={`/address/${item.id}/view`}>
          <section className="card-section">
            <strong>
              {item.label}
            </strong>
            <div>
              {item.address}, {item.number} Bairro: {item.neighborhood}
            </div>
            <div>
              CEP: {item.zipCode}
            </div>
          </section>
        </Link>
        <div className={`actions right ${showActions ? 'show' : ''}`}>
          <button
            className='btn toggleAction'
            onClick={() => this.toggleActions()}
          >
            {showActions ? '❯' : '❮'}
          </button>
          <Link className='btn edit' to={`/address/${item.id}/edit`}>
            Editar
          </Link>
          <div className='btn remove' onClick={() => onDelete()}>
            Excluir
          </div>
        </div>
      </div>
    )
  }
}
export default AddressItem;
