import React, {PropTypes} from 'react';
import './styles/ChecklistItem.css';

const ChecklistItem = ({item, onClick}) => (
  <div className='checklist-item' onClick={onClick}>
    <div className='icon'>
      {item.available ? <span>&#10004;</span> : null} 
    </div>
    {item.name}
  </div>
);

ChecklistItem.propTypes = {
  item: PropTypes.object.isRequired
};

export default ChecklistItem;
