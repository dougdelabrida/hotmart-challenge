import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FetchAddresses, DeleteAddress } from '../../actions/address';
import AddressItem from './components/AddressItem';
import AuthInterceptor from '../AuthInterceptor';
import TopBar from './components/TopBar';
import { Link } from 'react-router';

class AddressList extends React.Component {

  fetchAddresses = () => {
    const { actions, location } = this.props;
    actions.FetchAddresses(location.query);
  }

  deleteAddress = (id, index) => {
    if (confirm('Tem certeza que deseja excluir?')){
      this.props.actions.DeleteAddress(id, index);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { actions, location } = this.props
    if (nextProps.location.query.page !== location.query.page) {
      actions.FetchAddresses(nextProps.location.query);
    }
  }

  componentWillMount() {
    const { location, router } = this.props;
    if (!location.search) {
      router.push('/address?page=1&rows=2');
    }
  }

  render() {
    const { location } = this.props;
    const { isFetching, data, size } = this.props.address;

    const pages = () => {
      var pages = [];
      for(var i = 1; i < Math.round(size / parseInt(location.query.rows, 10)) + 1; i++) pages.push(i);
      return pages;
    }

    return (
      <AuthInterceptor router={this.props.router} onComponentWillMount={() => this.fetchAddresses() }>
        <TopBar />
        <div className='row'>
          <div className='small-12 medium-8 large-10 small-centered columns'>
            <h2>Address List</h2>
            <Link to='/address/add'>
              <div className='button'>Adicionar endereço</div>
            </Link>
            <div>
              {isFetching ? <div className='loader'>Loading...</div> :
              data.map((address, index) => <AddressItem key={address.id} item={address} onDelete={() => this.deleteAddress(address.id, index) } />) }
            </div>
            <ul className='pagination'>
              {pages().map(page => (
                <li key={page}
                  className={page === parseInt(location.query.page, 10) ? 'current' : null}
                >
                  <Link to={`/address?page=${page}&rows=${location.query.rows}`}>
                    {page}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </AuthInterceptor>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    address: state.address
  };
}

function mapDispatchToProps(dispatch) {
    const addressActions = {FetchAddresses, DeleteAddress};
    return {
        actions: bindActionCreators(addressActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressList);
