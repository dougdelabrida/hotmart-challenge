import React from 'react';
import TopBar from './components/TopBar';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AddAddress, EditAddress, ViewAddress } from '../../actions/address';
import AuthInterceptor from '../AuthInterceptor';
import ChecklistItem from './components/ChecklistItem';

class HandleAddress extends React.Component {

  state = {
    selected: {},
    message: {}
  }

  viewAddress = () => {
    const { add, actions, params } = this.props;
    if (add === undefined && params) actions.ViewAddress(params.id);
  }

  onSave(e) {
    e.preventDefault();
    const { add, edit, address } = this.props;
    const { checklistItems } = address.selected;
    const inputsHTMLCollection = [...e.target.getElementsByTagName('input')];
    const addressEdited = {};

    inputsHTMLCollection.forEach(({ name, value }) => addressEdited[name] = value);


    if (add) {
      // Get the latitude and longitude
      addressEdited.latitude = 0;
      addressEdited.longitude = 0;
      this.props.actions.AddAddress(addressEdited);
    }

    if (edit) {
      // I couldn't think a better way to do this.
      addressEdited.availableItems = checklistItems.reduce((result, item) => {
        if (item.available) result.push(item.id);
        return result;
      }, []);
      this.props.actions.EditAddress(addressEdited);
    }
  }

  toggleChecklistItem = (item, index) => {
    let availableItems = [...this.state.selected.checklistItems];
    item.available = !item.available;
    availableItems = availableItems[index] = item;
    this.setState({availableItems});
  }

  closeMessage = (e) => {
    e.preventDefault();
    this.setState({message: {}});
  }

  componentWillReceiveProps(nextProps) {
    const { selected, message } = nextProps.address;
    if (message !== this.state.message) {
      this.setState({message});
    }
    if (selected) {
      this.setState({selected})
    }
  }

  componentWillUnmount() {
    this.setState({selected: {}})
  }

  render() {
    const { router, add, view, edit } = this.props
    const { isFetching } = this.props.address
    const { selected, message } = this.state;

    return (
      <AuthInterceptor router={router} onComponentWillMount={() => this.viewAddress()}>
        <TopBar backTo='/address' />
        {isFetching ? <div className='loader'>Loading...</div> :
        <div className='row'>
          <div className='small-12 medium-8 large-10 small-centered columns'>
            <div className={`message ${message.text ? 'show' : null} ${message.type}`}>
              {message.text}
              <a href="#" className='close' onClick={(e) => this.closeMessage(e)}>&times;</a>
            </div>
            <form onSubmit={(e) => this.onSave(e) }>
              <input type='hidden' name='id' defaultValue={selected.id} />
              <div className='medium-12 columns'>
                <label>Nome
                  <input
                    type='text'
                    name='label'
                    placeholder='Ex: Meu trabalho'
                    defaultValue={selected.label}
                    readOnly={view}
                  />
                </label>
              </div>
              {view || add ?
              <div>
                <div className='medium-10 small-8 columns'>
                  <label>Endereço
                    <input
                      type='text'
                      name='address'
                      placeholder='Av. Prudente'
                      defaultValue={selected.address}
                      readOnly={view}
                    />
                  </label>
                </div>
                <div className='medium-2 small-4 columns'>
                  <label>N
                    <input
                      type='text'
                      name='number'
                      placeholder='123'
                      defaultValue={selected.number}
                      readOnly={view}
                    />
                  </label>
                </div>
                <div className='medium-6 large-4 columns'>
                  <label>Bairro
                    <input
                      type='text'
                      name='neighborhood'
                      placeholder='Serra Verde'
                      defaultValue={selected.neighborhood}
                      readOnly={view}
                    />
                  </label>
                </div>
                <div className='medium-6 large-4 columns'>
                  <label>Cidade
                    <input
                      type='text'
                      name='city'
                      placeholder='Campinas'
                      defaultValue={selected.city}
                      readOnly={view}
                    />
                  </label>
                </div>
                <div className='large-2 medium-8 columns'>
                  <label>Estado
                    <input
                      type='text'
                      name='state'
                      placeholder='São Paulo'
                      defaultValue={selected.state}
                      readOnly={view}
                    />
                  </label>
                </div>
                <div className='large-2 medium-4 columns'>
                  <label>CEP
                    <input
                      type='text'
                      name='zipCode'
                      placeholder='00000-00'
                      defaultValue={selected.zipCode}
                      readOnly={view}
                    />
                  </label>
                </div>
                <input type='hidden' name='country' defaultValue='Brasil' />
              </div>
              : null }
              <div className='medium-12 columns'>
                {selected.checklistItems && !add ?
                  selected.checklistItems.map((item, i) => (
                  <ChecklistItem
                    key={item.id}
                    item={item}
                    onClick={() => edit ? this.toggleChecklistItem(item, i) : null}
                  />
                ))
                : null}
              </div>
              <div className='medium-12 columns bottom-form'>
                <button
                  className='button'
                  type='submit'
                  onClick={() => view ? router.push(`/address/${selected.id}/edit`) : null }
                >
                  {edit || add ? 'Salvar' : 'Habilitar edição'}
                </button>
              </div>
            </form>
          </div>
        </div>
        }
      </AuthInterceptor>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    address: state.address
  };
}

function mapDispatchToProps(dispatch) {
    const addressActions = {AddAddress, EditAddress, ViewAddress};
    return {
        actions: bindActionCreators(addressActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HandleAddress);
