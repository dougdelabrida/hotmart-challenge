import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as userActions from '../../actions/user';
import './Login.css';

class Login extends React.Component {
  state = {
    emailInput: {},
    passwordInput: {}
  };

  handleEmail = (e) => {
    const {value, validity} = e.target;
    this.setState({emailInput: {value, validity}});
  }

  handlePassword = (e) => {
    const {value, validity} = e.target;
    this.setState({passwordInput: {value, validity}});
  }

  onLogin = (e) => {
    e.preventDefault();
    const { emailInput, passwordInput } = this.state;
    this.props.actions.Login(
      {
        email: emailInput.value,
        password: passwordInput.value
      }
    )
  }

  componentDidUpdate() {
    if(this.props.user.isAuth) {
      this.props.router.push('/');
    }
  }

  render() {

    const { isFetching, error } = this.props.user;
    const { emailInput, passwordInput } = this.state;
    const disabled = emailInput.validity && passwordInput.validity && (!emailInput.validity.valid || !passwordInput.validity.valid);

    return (
      <div>
        <div className='login-box'>
          <h2>Login Page</h2>
          <form onSubmit={(e) => this.onLogin(e) }>
            <div>
              <span style={{display: error.message ? 'block' : 'none'}}>{error.message}</span>
            </div>
            <div>
              <small style={{display: emailInput.validity && emailInput.validity.typeMismatch ? 'block' : 'none'}}>E-mail inválido</small>
              <input type='email' onChange={(e) => this.handleEmail(e)} placeholder='E-mail' required disabled={isFetching}/>
            </div>
            <div>
              <input type='password' onChange={(e) => this.handlePassword(e)} placeholder='Senha' required disabled={isFetching}/>
            </div>
            <div>
              <button className='button' type='submit' disabled={disabled || isFetching}>{isFetching ? 'Entrando' : 'Entrar'}</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
