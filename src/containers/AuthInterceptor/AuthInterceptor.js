import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Logout } from '../../actions/user';
import fetchIntercep from 'fetch-intercept';

import conf from '../../config';

class AuthInterceptor extends React.Component {

  interceptorUnregister;

  componentDidUpdate() {
    const { user, router } = this.props;
    if (!user.isAuth) {
      router.push('/login');
    }
  }

  componentWillMount() {
    const {user, onComponentWillMount, router} = this.props;
    if (user.isAuth) {
      this.interceptorUnregister = fetchIntercep.register({
        request: (url, config) => {
          if (!url.includes(conf.URL_API)) return [url, config];
          if (config.headers) {
            config.headers.Authorization = `Bearer ${user.authData.access_token}`;
          } else {
            config.headers = {Authorization: `Bearer ${user.authData.access_token}`};
          }
          return [url, config];
        },
        response: function (response) {
          if (response.status === 401 || response.status === 400) {
            // 400 Bad Request? Token inválido
            localStorage.removeItem('auth_data');
            location.reload();
          }
          return response;
        }
      });

      if (onComponentWillMount) {
        onComponentWillMount();
      }

    } else {
      router.push('/login');
    }
  }

  componentWillUnmount() {
    if (this.interceptorUnregister) {
      this.interceptorUnregister();
    }
  }

  render() {
    return (
      <div>
        {this.props.children}
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
    const userActions = {Logout};
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

AuthInterceptor.propTypes = {
  router: React.PropTypes.object.isRequired,
  onComponentWillMount: React.PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthInterceptor);
