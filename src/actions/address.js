export const FETCH_REQUEST = 'FETCH_REQUEST';
export const RECEIVE_ADDRESSES = 'RECEIVE_ADDRESSES';
export const RECEIVE_ADDRESS = 'RECEIVE_ADDRESS';
export const EDIT_ADDRESS_SUCCESS = 'EDIT_ADDRESS_SUCCESS';
export const ADD_ADDRESS_SUCCESS = 'ADD_ADDRESS_SUCCESS';
export const ADD_ADDRESS_ERROR = 'ADD_ADDRESS_ERROR';
export const DELETE_ADDRESS_SUCCESS = 'DELETE_ADDRESS_SUCCESS';
export const DELETE_ADDRESS_ERROR = 'DELETE_ADDRESS_ERROR';

import config from '../config';

export const FetchAddresses = ({ page = 1, rows = 2 }) => {
  return dispatch => {
    dispatch(FetchRequest());
    fetch(`${config.URL_API}/hack-dragonfly/rest/v1/address?page=${page}&rows=${rows}`, { method: 'GET' }).then(response => {
      if (response.status === 200) {
        response.json().then(result => {
          dispatch(ReceiveAddresses(result));
        })
      }
    });
  }
}

export const ViewAddress = (id) => {
  return dispatch => {
    dispatch(FetchRequest());
    fetch(`${config.URL_API}/hack-dragonfly/rest/v1/address/${id}`, { method: 'GET' }).then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          dispatch(ReceiveAddress(data));
        })
      }
    });
  }
}

export const AddAddress = (address) => {
  return dispatch => {
    dispatch(FetchRequest());
    fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=
           ${address.number}+${address.address}+${address.neighborhood},+${address.city},
           ${address.state}+${address.zipCode}&key=${config.MAPS_API_KEY}`,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json'
        }
      }
    ).then(response => {
      if (response.status === 200) {
        response.json().then(({ results }) => {
          if (results.length === 0) throw Error('Erro ao determinar latitude e longitude');

          const { lat, lng } = results[0].geometry.location;

          address.latitude = lat;
          address.longitude = lng;

          fetch(`${config.URL_API}/hack-dragonfly/rest/v1/address`,
          {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(address)
          }).then(response => {
            if (response.status === 201) {
              response.json().then(data => {
                dispatch(AddAddressSuccess(data));
              });
            }
          });
        }).catch(err => {
          dispatch(AddAddressError(err.message));
        });
      }
    })
  };
}

export const EditAddress = (address) => {
  return dispatch => {
    dispatch(FetchRequest());
    fetch(`${config.URL_API}/hack-dragonfly/rest/v1/address/${address.id}`,
    {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(address)
    }).then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          dispatch(EditAddressSuccess(address));
        });
      }
    });
  };
}

export const DeleteAddress = (id, index) => {
  return dispatch => {
    fetch(`${config.URL_API}/hack-dragonfly/rest/v1/address/${id}`,
      {
        method: 'DELETE'
      }
    ).then(response => {
      if(response.status === 200) {
        dispatch(DeleteAddressRequest(index));
      }
    });
  };
}

function FetchRequest() {
  return {
    type: FETCH_REQUEST
  }
}

function ReceiveAddresses(result) {
  return {
    type: RECEIVE_ADDRESSES,
    result
  }
}

function ReceiveAddress(data) {
  return {
    type: RECEIVE_ADDRESS,
    data
  }
}

function EditAddressSuccess(address) {
  return {
    type: EDIT_ADDRESS_SUCCESS,
    address
  }
}

function AddAddressSuccess(address) {
  return {
    type: ADD_ADDRESS_SUCCESS,
    address
  }
}

function AddAddressError(message) {
  return {
    type: ADD_ADDRESS_ERROR,
    message
  }
}

function DeleteAddressRequest(index) {
  return {
    type: DELETE_ADDRESS_SUCCESS,
    message: `Endereço removido com sucesso`,
    index
  }
}
