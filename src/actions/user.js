export const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';

import config from '../config';

export const Login = (user) => {
  return dispatch => {
    dispatch(LoginRequest(user));
    fetch(`http://api-hck.hotmart.com/security/oauth/token?grant_type=password&username=${user.email}&password=${user.password}`,
      {
        method: 'POST',
        headers: {
          Authorization: config.HOT_TOKEN
        }
      }).then(response => {
          if(response.status === 200) {
            response.json().then(data => {
              dispatch(LoginSuccess(data));
            });
          } else {
            dispatch(LoginFailure({message: 'Usuário ou(e) senha inválidos.'}));
          }
        }).catch(err => {
          dispatch(LoginFailure({message: 'Erro ao comunicar com o servidor.'}));
      })
  };
}

function LoginRequest (user) {
  return {
    type: USER_LOGIN_REQUEST,
    user
  }
}

function LoginSuccess (data) {
  return {
      type: USER_LOGIN_SUCCESS,
      data
  };
}

function LoginFailure (error) {
  return {
      type: USER_LOGIN_FAILURE,
      error
  };
}
