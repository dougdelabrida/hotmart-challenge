import React, { Component } from 'react';
import { Router, Route, browserHistory, Redirect } from 'react-router';
import Login from './containers/Login';
import AddressList from './containers/Address/List';
import HandleAddress from './containers/Address/Handle';

class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Redirect from="/" to="/address" />
        <Route path='/address' component={AddressList} />
        <Route path='/address/add' component={(props) => <HandleAddress {...props} add/>} />
        <Route path='/address/:id/edit' component={(props) => <HandleAddress {...props} edit/>} />
        <Route path='/address/:id/view' component={(props) => <HandleAddress {...props} view/>} />
        <Route path='/login' component={Login} />
      </Router>
    );
  }
}

export default App;
