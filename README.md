# Address App

#### How to run it?

1. Set your settings at config.js

  - URL_API
  - MAPS_API_KEY
  - HOT_TOKEN (Provided by Hotmart)

2. Install dependecies ( Nodejs required )

```
 npm install
 ```

3. Finally at the project folder, run the command

```
  npm start
```
  
#### Build for deploy
```
 npm build
 ```